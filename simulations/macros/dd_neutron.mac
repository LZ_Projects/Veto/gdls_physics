#########################################################
# Title: dd_neutron.mac
# Author: Sam Eriksen
# BACCARAT: release 6.2.1
# Sources: DD neutron (2.45MeV)
# Backgrounds: None
# BeamOn: 1
#########################################################

/run/verbose       0
/control/verbose   0
/tracking/verbose  1
/grdm/verbose      0

#########################################################
# IO
#########################################################
/Bacc/io/enableRootOutput 0
/Bacc/io/enableBinaryOutput 1

/Bacc/io/outputDir .

/Bacc/io/outputName DD_sims

/Bacc/io/alwaysRecordPrimary true

/Bacc/io/updateFrequency  1

#########################################################
# Set Record Levels: New ROOT Format
#########################################################
/Bacc/io/setVolumeToRecord LiquidXenonTarget record=deposits,volumes
/Bacc/io/setVolumeToRecord BottomGridHolder record=deposits,volumes
/Bacc/io/setVolumeToRecord ReverseFieldRegion record=deposits,volumes
/Bacc/io/setVolumeToRecord CathodeGridHolder record=deposits,volumes
/Bacc/io/setVolumeToRecord GateGridHolder record=deposits,volumes
/Bacc/io/setVolumeToRecord LiquidSkinXenon record=deposits,volumes
/Bacc/io/setVolumeToRecord LiquidSkinXenonBank record=deposits,volumes
/Bacc/io/setVolumeToRecord ScintillatorCenter record=deposits,volumes
/Bacc/io/setVolumeToRecord InnerGaseousXenon record=deposits,volumes
/Bacc/io/setVolumeToRecord AnodeGridHolder record=deposits,volumes
/Bacc/io/setVolumeToRecord TopGridHolder record=deposits,volumes
/Bacc/io/setVolumeToRecord GaseousSkinXenon record=deposits,volumes
/Bacc/io/setVolumeToRecord GaseousSkinXenonBank record=deposits,volumes

#########################################################
# Physics list
#########################################################
/Bacc/physicsList/useOptics true
/Bacc/physicsList/maxCereEleSteps 1000

#/Bacc/physicsList/Optics/setQeGains true
#/Bacc/physicsList/Optics/scintPhotonGain 0.4
#/Bacc/physicsList/Optics/cherenkovPhotonGain 0.4
#/Bacc/physicsList/Optics/s1PhotonGain 0.4

/Bacc/physicsList/genTimesToZero true

/Bacc/physicsList/usePhotonEvaporation true

/run/initialize

#########################################################
# Set the seed
#########################################################
/Bacc/randomSeed 10

#########################################################
# Detector setup
#########################################################
/Bacc/detector/select LZDetector

/LZ/gridWires off

/LZ/StraightNeutronTubeEmpty true
/Bacc/detector/update

#########################################################
# Material commands
#########################################################
/Bacc/materials/LXeTeflonRefl          0.973
/Bacc/materials/LXeAbsorption          100 m
/Bacc/materials/LXeSteelRefl           0.2
/Bacc/materials/GXeTeflonRefl          0.85
/Bacc/materials/GXeBlackWallRefl       0.2
/Bacc/materials/GXeSteelRefl           0.2
/Bacc/materials/GXeAbsorption          500 m
/Bacc/materials/AlUnoxidizedQuartzRefl 0.9
/Bacc/materials/GXeRayleigh            500 m
/Bacc/materials/LXeRayleigh            0.3 m
/Bacc/materials/QuartzAbsorption       1000 km
/Bacc/materials/LXeTeflonReflLiner    0.98
/Bacc/materials/LXeTeflonReflPMT      0.95
/Bacc/materials/LXeTeflonReflPlate    0.9
/Bacc/materials/LXeTeflonReflCables   0.1
/Bacc/materials/LXeTeflonReflTruss    0.8
/Bacc/materials/LXeTeflonReflBskinPMT 0.8
/Bacc/materials/LXeTeflonReflBplate   0.9
/Bacc/materials/LXeTeflonReflTskinPMT 0.8
/Bacc/materials/LXeTeflonReflWeir     0.1
/Bacc/materials/LXeTeflonReflDomePMT  0.8

#########################################################
# Set the Record Levels: Old BIN Format
#########################################################
/Bacc/detector/useMapOptical true

#########################################################
# Masses
#########################################################
/Bacc/detector/setComponentMass LiquidXenonTarget 7000 kg
# Xe Masses
/Bacc/detector/setComponentMass LiquidSkinXenon 1951.333 kg
/Bacc/detector/setComponentMass GaseousSkinXenon 1.95 kg
/Bacc/detector/setComponentMass GaseousSkinXenonBank 0.65 kg
/Bacc/detector/setComponentMass LiquidSkinXenonBank 650.419 kg
/Bacc/detector/setComponentMass LiquidXenonTarget 7000.410 kg
/Bacc/detector/setComponentMass InnerGaseousXenon 2.80 kg
/Bacc/detector/setComponentMass ReverseFieldRegion 659.457 kg
/Bacc/detector/setComponentMass GateGridHolder 0.736 kg
/Bacc/detector/setComponentMass CathodeGridHolder 0.976 kg
/Bacc/detector/setComponentMass BottomGridHolder 0.736 kg
/Bacc/detector/setComponentMass AnodeGridHolder 5.07E-03 kg
/Bacc/detector/setComponentMass ScintillatorCenter 17300 kg
# PMT Masses
/Bacc/detector/setComponentMass Top_PMT_Ceramic 1.570E-02 kg
/Bacc/detector/setComponentMass Bottom_PMT_Ceramic 1.570E-02 kg
/Bacc/detector/setComponentMass Top_PMT_Body 1.317E-01 kg
/Bacc/detector/setComponentMass Bottom_PMT_Body 1.317E-01 kg
/Bacc/detector/setComponentMass Top_PMT_Base 5.661E-03 kg
/Bacc/detector/setComponentMass Bottom_PMT_Base 5.661E-03 kg
/Bacc/detector/setComponentMass Dome_Skin_PMT_Body 1.267E-01 kg
/Bacc/detector/setComponentMass Bottom_Skin_PMT_Body 1.267E-01 kg
/Bacc/detector/setComponentMass Top_Skin_PMT_Body 2.397E-02 kg
/Bacc/detector/setComponentMass Water_PMT_Window 1.02 kg
/Bacc/detector/setComponentMass Water_PMT_Can 0.686 kg
# PTFE Masses
/Bacc/detector/setComponentMass PTFEWallsInLiquid 184.0 kg
/Bacc/detector/setComponentMass PTFELinerLiquid 26.224 kg #Skin teflon outer liner
/Bacc/detector/setComponentMass TopPTFELiner 5.0278 kg #Upper PMT Structure -> Trifoils, PMT sleeves etc. for top array
/Bacc/detector/setComponentMass BottomPTFELiner 1.3527 kg #Lower PMT Structure -> Trifoils  for bottom array
/Bacc/detector/setComponentMass BottomXeSkinPTFELiner 66.7273 kg #Lower PMT Structure -> Dome skin PODs etc. pointing into skin dome
/Bacc/detector/setComponentMass TopSkinIceCubeTray 10.565 kg #This is *not* in the backgrounds table. Going to assume the same specs as TopPTFELiner
# Cryostat Masses
/Bacc/detector/setComponentMass InnerTitaniumVessel 800 kg
/Bacc/detector/setComponentMass OuterTitaniumVessel 1022 kg
/Bacc/detector/setComponentMass TitaniumLegs 454 kg
/Bacc/detector/setComponentMass FoamInsulation 13.781 kg
/Bacc/detector/setComponentMass AluminumSealantInInnerVesselFlange 7.37E-01 kg #(Al+Iconel+Niconic) The following are guesses,
/Bacc/detector/setComponentMass ElastomerSealantInInnerVesselFlange 2.29E-01 kg  #using the previous ratio of masses to divide
/Bacc/detector/setComponentMass ElastomerSealantInOuterVesselUpperFlange 5.74E-01 kg #up mass from backgrounds table
/Bacc/detector/setComponentMass ElastomerSealantInOuterVesselBottomFlange 5.74E-01 kg
/Bacc/detector/setComponentMass BoltsInInnerVesselFlange 23 kg #Bolts take on all 53.44 kg of Stainless Steel...
/Bacc/detector/setComponentMass BoltsInOuterVesselUpperFlange 15.22 kg
/Bacc/detector/setComponentMass BoltsInOuterVesselBottomFlange 15.22 kg
# Truss Masses - these absorb everything for lower/upper PMT structure, including PTFE mass that is separately used for PTFEAlphaN
/Bacc/detector/setComponentMass BottomTruss 53.625 kg
/Bacc/detector/setComponentMass TopTruss 115.143 kg
# Conduit Mass - using this to represent the entire Xe recirculation tubing
/Bacc/detector/setComponentMass BottomConduit 14.09 kg
# Outer Detector Masses
/Bacc/detector/setComponentMass ScintillatorTank 3164 kg #Acrylic + Displacement Foam
/Bacc/detector/setComponentMass SteelLegs 1100 kg
# Field Ring Masses
/Bacc/detector/setComponentMass ForwardFieldRing 1.3464 kg
/Bacc/detector/setComponentMass ReverseFieldRing 1.35 kg
# Grid Holder Masses
/Bacc/detector/setComponentMass AnodePhysGrid 19.94 kg
/Bacc/detector/setComponentMass BottomPhysGrid 11.5 kg
/Bacc/detector/setComponentMass CathodePhysGrid 17.9 kg
/Bacc/detector/setComponentMass GatePhysGrid 33.35 kg
# HV Conduit Masses
/Bacc/detector/setComponentMass HVConduitVacuumInnerSteelCone 7.70 kg  #Going again by the ratio of inner to outer cones in the TDR macro
/Bacc/detector/setComponentMass HVConduitInnerOuterSteelCone 121.30 kg  #Going again by the ratio of inner to outer cones in the TDR macro
/Bacc/detector/setComponentMass HVConduitInnerConeInnerStructure 14.87 kg
/Bacc/detector/setComponentMass HVHolePTFELinerLiquid 0.905 kg

#########################################################
# Sources
#########################################################
/Bacc/source/set DD 0.001 mBq

#########################################################
# Start simulation
#########################################################
/Bacc/beamOn 100
exit
