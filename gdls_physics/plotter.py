import matplotlib.pyplot as plt
import numpy as np


def plot_bar(ax, values, make_unique=True, heights=[], x_label='', y_label='', title='', enlarge_x_ticks=False,
             label=''):
    """
    Add a bar plot to some axis.


    :param ax: axis to add plot to
    :type ax: matplotlib.pyplot.axis
    :param values: a 1D array or list
    :type values: np.array or list
    :param make_unique: if True, manually create a histogram of unique values and weights.
    If False, use heights as weight and values as the bins.
    :type make_unique: bool
    :param heights: weights
    :type heights: list or np.array
    :param x_label: axis xlabel
    :type x_label: str
    :param y_label: axis ylabel
    :type y_label: str
    :param title: axis title
    :type title: str
    :param enlarge_x_ticks:
    :type enlarge_x_ticks: bool
    :param label: bar label
    :type label: str
    :return:
    :rtype: None
    """

    if make_unique:
        uniques = np.unique(values)
        weights = []
        for uni in uniques:
            count = 0
            for j in values:
                if j == uni:
                    count += 1
            weights.append(count)
        ax.bar(uniques, weights, label=label)
    else:
        if len(heights) != len(values):
            heights = np.zeros(len(values))
        ax.bar(values, heights, label=label)

    ax.set_ylabel(y_label, size=24)
    ax.set_xlabel(x_label, size=24)
    ax.set_title(title, size=24)
    if enlarge_x_ticks:
        ax.tick_params(axis='x', labelsize=20)
