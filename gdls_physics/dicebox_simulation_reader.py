import glob
import gdls_physics
import time
import matplotlib.pyplot as plt
from . import progress_bars


def dicebox_file_loop(file):
    """
    Open Dicebox simulation txt file and extract information.

    :param file: location of file to open
    :type file: str
    :return:
    :rtype: tuple[list[list[int]], list[list[float]]]
    """

    all_shells = []
    all_energies = []

    open_file = open(file, "r")
    for line in open_file:
        split_line = line.split()

        # Get shells
        shells = [int(s) for s in split_line[3::2]]
        # Get energy transfers
        energies = [float(e) for e in split_line[2::2]]

        # Add to all
        all_energies.append(energies)
        all_shells.append(shells)

    return all_shells, all_energies


def get_dicebox_simulation_results(search_location):
    """
    Loop dicebox simulation files and extract results

    :param search_location:
    :type search_location: str
    :return:
    :rtype: tuple[list[list[int]], list[list[float]]]
    """

    file_list = glob.glob(search_location)

    all_shells = []
    all_energies = []

    start_time = time.time()
    n_files = len(file_list)
    progress_bars.progress_update(0, n_files, start_time)
    for i, file in enumerate(file_list):
        shells, energies = dicebox_file_loop(file)
        all_shells.extend(shells)
        all_energies.extend(energies)

        if i % 10 == 0:
            progress_bars.progress_update(i + 1, n_files, start_time)
    progress_bars.progress_update(n_files, n_files, start_time)

    return all_shells, all_energies


def extract_gamma_electron_spectrum(shells, energies, separate_events=False):
    """
    Get the energy transition spectrums for gammas and IC electrons.

    :param shells:
    :type shells: list[list[int]]
    :param energies:
    :type energies: list[list[float]]
    :param separate_events:
    :type separate_events: bool
    :return: gamma and electron energy transfers
    :rtype: tuple[list[float], list[float]]
    """

    gamma_e = []
    electron_e = []
    for i, line_shell in enumerate(shells):
        these_gamma = []
        these_electron = []
        for j, shell in enumerate(line_shell):
            if shell == 0:
                these_gamma.append(energies[i][j])
            else:
                these_electron.append(energies[i][j])
        if separate_events:
            gamma_e.append(these_gamma)
            electron_e.append(these_electron)
        else:
            gamma_e.extend(these_gamma)
            electron_e.extend(these_electron)
    return gamma_e, electron_e


def get_gamma_electron_energy_fraction(shells, energies, transition_energy):
    """
    Get the fraction of energy from the de-excitation that is transfered to gammas and IC electrons

    :param shells:
    :type shells: list[list[int]]
    :param energies:
    :type energies: list[list[float]]
    :param transition_energy:
    :type transition_energy: float
    :return: gamma and electron energy transfers
    :rtype: tuple[list[float], list[float]]
    """

    gamma_frac = []
    electron_frac = []
    for i, line_shell in enumerate(shells):
        gamma_e = 0.0
        electron_e = 0.0
        for j, shell in enumerate(line_shell):
            if shell == 0:
                gamma_e += energies[i][j]
            else:
                electron_e += energies[i][j]
        gamma_frac.append(gamma_e / transition_energy * 100)
        electron_frac.append(electron_e / transition_energy * 100)
    return gamma_frac, electron_frac


def save_for_latex(weights, bins, filename, titles, data_type):
    """

    :param weights:
    :type weights: list or np.array
    :param bins:
    :type bins: list or np.array
    :param filename:
    :type filename: str
    :param titles:
    :type titles: str
    :param data_type:
    :type data_type: str
    :return:
    :rtype:
    """

    f = open(filename, 'w')
    f.write(titles)
    if data_type == 'hist':
        for i in range(len(bins) - 1):
            f.write(str(bins[i]) + '\t' + str(bins[i + 1]) + '\t' + str(weights[i]) + '\n')
    elif data_type == 'add_label_and_count':
        for i in range(len(bins)):
            f.write(str(bins[i]) + '\t' + str(i) + '\t' + str(weights[i]) + '\n')
    else:
        for i in range(len(bins)):
            f.write(str(bins[i]) + '\t' + str(weights[i]) + '\n')
    f.close()
