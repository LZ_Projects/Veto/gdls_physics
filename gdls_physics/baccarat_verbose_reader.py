import numpy as np
import treelib
import time
from . import progress_bars
import matplotlib.pyplot as plt


# noinspection DuplicatedCode
def examine_single_run_file(verbose_log_file_location):
    """
    Open a BACCARAT log file and extract the particle information from the first line.
    For example; the log file will contain a particle tracking information as below;

    :param verbose_log_file_location: location of BACCARAT output file
    :type verbose_log_file_location: str
    :return: list of particle information
    :rtype: tuple[np.ndarray[str], np.ndarray[int], np.ndarray[int], np.ndarray[str], np.ndarray[float]]

    .. note::
        This function is designed to look at BACCARAT log files which contain just one Run.
        It will work when there are more Runs, but the Runs will not be separated into a separate list.

    :Example:
        .. code-block:: sh

            *********************************************************************************************************
            * G4Track Information:   Particle = Xe131,   Track ID = 3,   Parent ID = 1
            *********************************************************************************************************

            Step#    X(mm)    Y(mm)    Z(mm) KinE(MeV)  dE(MeV) StepLeng TrackLeng  NextVolume ProcName
                0     -665   -0.597 1.38e+03    0.0509        0        0         0 LiquidXenonTarget initStep
                1     -665   -0.597 1.38e+03         0   0.0509 0.000443  0.000443 LiquidXenonTarget ionIoni
                2     -665   -0.597 1.38e+03         0        0        0  0.000443 LiquidXenonTarget S1

        In this case the function would extract the following and append to the relevant lists;

            * Particle = Xe131
            * Track ID = 3
            * Parent ID = 1
            * Step 0 NextVolume = LiquidXenonTarget
            * Step 0 KinE(MeV) = 0.0509

    """
    log_file = open(verbose_log_file_location, "r")
    particles = []
    track_ids = []
    parent_ids = []
    initiation_volume = []
    initiation_keV = []
    initiation_line = False
    for line in log_file:
        # Find first run
        if 'Run' in line and 'start' in line:
            print('Found Run')
            print(line)
            break
    # Now search for info
    for line in log_file:
        if 'Particle' in line and 'Track ID' in line:
            split_line = line.split()
            particles.append(split_line[5][:-1])
            track_ids.append(int(split_line[9][:-1]))
            parent_ids.append(int(split_line[-1]))
        if initiation_line:
            split_line = line.split()
            initiation_volume.append(split_line[-2])
            initiation_keV.append(float(split_line[4]))
            initiation_line = False
        if 'Step#' in line:
            initiation_line = True
    # Turn into numpy arrays
    particles = np.array(particles)
    track_ids = np.array(track_ids)
    parent_ids = np.array(parent_ids)
    initiation_volume = np.array(initiation_volume)
    initiation_keV = np.array(initiation_keV)

    return particles, track_ids, parent_ids, initiation_volume, initiation_keV


def optical_photon_track_reduction(particles, parent_ids):
    """
    Get the number of optical photons produced by each parent particle (which produced at least one optical photon).
    Return the parent track ids and number of optical photons produced.

    :param particles: list of str containing particle names
    :param parent_ids: list of int containing parent track ids
    :type particles: np.ndarray([str])
    :type parent_ids: np.ndarray([int])
    :return: list of parent track ids that produced optical photons and list of number of optical photons
    :rtype: tuple[list[int], list[int]]
    """
    optical_photons_parent_id = []
    for i in range(len(parent_ids)):
        if particles[i] == 'opticalphoton':
            optical_photons_parent_id.append(parent_ids[i])
    print('Extracted optical photon tracks')
    n_photons = []
    photon_tracks = []
    unique_ids = np.unique(optical_photons_parent_id)
    start_time = time.time()
    i = 0
    for i in range(len(unique_ids)):
        these_photon_id = np.where(optical_photons_parent_id == unique_ids[i])[0]
        n_photons.append(len(these_photon_id))
        photon_tracks.append(unique_ids[i])
        if i % 5000 == 0:
            progress_bars.progress_update(i, len(unique_ids), start_time)
    progress_bars.progress_update(i, len(unique_ids), start_time)
    print('Done')
    return photon_tracks, n_photons


class ParticleProperties(object):
    """
    Create an object containing a particles properties.

    Each object is a treelib.Node().

    :param energy: particles energy
    :type energy: int, float
    """

    def __init__(self, energy):
        self.energy = str(energy)


def create_single_run_tree(particles, track_ids, parent_ids, initiation_volume, initiation_keV, reduce_photons=True,
                           photon_tracks=np.array([]), n_photons=np.array([]), volume_selection=True):
    """
    Create tree of particle cascade.

    :param particles: array of particle names
    :param track_ids: array of particle track id
    :param parent_ids: array of particle parent track ids
    :param initiation_volume: array of particle initial volumes
    :param initiation_keV: array of particle initial kinetic energy
    :param reduce_photons: if True, put number of optical photons rather than one entry per optical photon.
    :param photon_tracks: array of optical photon parent track ids
    :param n_photons: number of photons produced from parent track (matches up with photon_tracks)
    :param volume_selection: if True, select just GdLS events (events where the initial volume contains 'Scintillator')
    :type particles: np.array
    :type track_ids: np.array
    :type parent_ids: np.array
    :type initiation_volume: np.array
    :type initiation_keV: np.array
    :return: data tree where each node is a particle containing the name and energy.
    :rtype: treelib.Tree
    """
    done_optical_tracks = []
    n_entries = len(parent_ids)
    start_time = time.time()
    progress_bars.progress_update(0, n_entries, start_time)
    tree = treelib.Tree()
    progress_particle = True
    i = 0

    for i in range(len(parent_ids)):
        this_particle_info = [parent_ids[i], particles[i], track_ids[i], initiation_volume[i], initiation_keV[i]]
        if i == 0:
            # create root node
            print(i, this_particle_info[2])
            tree.create_node(this_particle_info[1], this_particle_info[2],
                             data=ParticleProperties(this_particle_info[-1]))
        else:
            if volume_selection:
                if 'Scintillator' in this_particle_info[3]:
                    progress_particle = True
                else:
                    progress_particle = False
            if progress_particle:
                if this_particle_info[1] == 'opticalphoton':
                    if reduce_photons:
                        if this_particle_info[0] not in done_optical_tracks:
                            try:
                                node_photons = n_photons[np.where(photon_tracks == this_particle_info[0])[0][0]]
                                tree.create_node(str(node_photons) + ' opticalphoton', this_particle_info[2],
                                                 parent=this_particle_info[0],
                                                 data=ParticleProperties(this_particle_info[-1]))
                                done_optical_tracks.append(this_particle_info[0])
                            except:  # treelib.exceptions.NodeIDAbsentError:
                                continue
                    else:
                        try:
                            tree.create_node(this_particle_info[1], this_particle_info[2], parent=this_particle_info[0],
                                             data=ParticleProperties(this_particle_info[-1]))
                        except:  # treelib.exceptions.NodeIDAbsentError:
                            continue
                else:
                    try:
                        tree.create_node(this_particle_info[1], this_particle_info[2], parent=this_particle_info[0],
                                         data=ParticleProperties(this_particle_info[-1]))
                    except:  # treelib.exceptions.NodeIDAbsentError:
                        continue
        if i % 5000 == 0:
            progress_bars.progress_update(i, n_entries, start_time)
    progress_bars.progress_update(i, n_entries, start_time)
    print('Done')
    return tree


def get_neutron_scintillation_energy(file_list):
    """

    :param file_list: list of files
    :type file_list: list[str]
    :return:
    :rtype: list[float]
    """

    neutron_scint_keV = []
    for f in file_list:
        particles, track_ids, parent_ids, initiation_volume, initiation_keV = examine_single_run_file(f)
        # get neutrons
        neutrons_ids = np.where(particles == 'neutron')
        neutron_initial_volumes = initiation_volume[neutrons_ids]
        neutron_keV = initiation_keV[neutrons_ids]
        for i, volume in enumerate(neutron_initial_volumes):
            if 'Scintillator' in volume:
                neutron_scint_keV.append(float(neutron_keV[i]))

    return neutron_scint_keV


def examine_multirun_run_file(verbose_log_file_location):
    """
    Open a BACCARAT log file and extract the particle information from the first line.
    For example; the log file will contain a particle tracking information as below;

    :param verbose_log_file_location: location of BACCARAT output file
    :type verbose_log_file_location: str
    :return: list of particle information
    :rtype: tuple[np.ndarray[np.ndarray[str]], np.ndarray[np.ndarray[int]], np.ndarray[np.ndarray[int]],
                  np.ndarray[np.ndarray[str]], np.ndarray[np.ndarray[float]]]

    .. note::
        This function is designed to look at BACCARAT log files which contain more than one Run.
        It has not been tested when looking at more than one Run

    :Example:
        .. code-block:: sh

            *********************************************************************************************************
            * G4Track Information:   Particle = Xe131,   Track ID = 3,   Parent ID = 1
            *********************************************************************************************************

            Step#    X(mm)    Y(mm)    Z(mm) KinE(MeV)  dE(MeV) StepLeng TrackLeng  NextVolume ProcName
                0     -665   -0.597 1.38e+03    0.0509        0        0         0 LiquidXenonTarget initStep
                1     -665   -0.597 1.38e+03         0   0.0509 0.000443  0.000443 LiquidXenonTarget ionIoni
                2     -665   -0.597 1.38e+03         0        0        0  0.000443 LiquidXenonTarget S1

        In this case the function would extract the following and append to the relevant lists;

            * Particle = Xe131
            * Track ID = 3
            * Parent ID = 1
            * Step 0 NextVolume = LiquidXenonTarget
            * Step 0 KinE(MeV) = 0.0509


    """
    log_file = open(verbose_log_file_location, "r")
    particles = []
    this_run_particles = []
    track_ids = []
    this_run_track_ids = []
    parent_ids = []
    this_run_parent_ids = []
    initiation_volume = []
    this_run_initiation_volume = []
    initiation_keV = []
    this_run_initiation_keV = []
    initiation_line = False
    for line in log_file:
        # Find first run
        if 'Run' in line and 'start' in line:
            print('Found Run Start')
            print(line)
            break
    # Now search for info pf each run
    for line in log_file:
        if 'Particle' in line and 'Track ID' in line:
            split_line = line.split()
            this_run_particles.append(split_line[5][:-1])
            this_run_track_ids.append(int(split_line[9][:-1]))
            this_run_parent_ids.append(int(split_line[-1]))
        if initiation_line:
            split_line = line.split()
            this_run_initiation_volume.append(split_line[-2])
            this_run_initiation_keV.append(float(split_line[4]))
            initiation_line = False
        if 'Step#' in line:
            initiation_line = True
        if 'Processing event' in line:
            particles.append(np.array(this_run_particles))
            this_run_particles = []
            track_ids.append(np.array(this_run_track_ids))
            this_run_track_ids = []
            parent_ids.append(np.array(this_run_parent_ids))
            this_run_parent_ids = []
            initiation_volume.append(np.array(this_run_initiation_volume))
            this_run_initiation_volume = []
            initiation_keV.append(np.array(this_run_initiation_keV))
            this_run_initiation_keV = []
    # Turn into numpy arrays
    particles = np.array(particles)
    track_ids = np.array(track_ids)
    parent_ids = np.array(parent_ids)
    initiation_volume = np.array(initiation_volume)
    initiation_keV = np.array(initiation_keV)
    return particles, track_ids, parent_ids, initiation_volume, initiation_keV


def extract_event_log_information(log_file):
    """
    For each particle in an event, extract the event information.
    Everything between the initial :code:`*******` and the :code:`******` of the next particle is included; errors and
    all.

    If the event contains multiple events, then they will be separated into separate in a higher order list, of the
    structure; :code:`event_list[particle_list[particle_info_list[particle_info_str]]`.

    :Example:
        A string similar to the one show below will be created for each particle. Where each line is a new entry in
        the list.
        .. code-block:: sh

            *********************************************************************************************************
            * G4Track Information:   Particle = Xe131,   Track ID = 3,   Parent ID = 1
            *********************************************************************************************************

            Step#    X(mm)    Y(mm)    Z(mm) KinE(MeV)  dE(MeV) StepLeng TrackLeng  NextVolume ProcName
                0     -665   -0.597 1.38e+03    0.0509        0        0         0 LiquidXenonTarget initStep
                1     -665   -0.597 1.38e+03         0   0.0509 0.000443  0.000443 LiquidXenonTarget ionIoni
                2     -665   -0.597 1.38e+03         0        0        0  0.000443 LiquidXenonTarget S1


    :param log_file: location of the log file
    :type log_file: str
    :return: A list of strings for each particle
    :rtype: list[list[list[str]]] or list[list[str]]
    """
    f = open(log_file, "r")
    particles = []
    this_particle = []
    all_events_particles = []
    for line in f:
        # Find first run
        if 'Run' in line and 'start' in line:
            print('Found Run')
            print(line)
            break
    # Now search for info
    prev_line = line
    ignore_line = False
    first_particle = True
    for line in f:
        if '*********' in line and prev_line == '\n':
            particles.append(this_particle)
            this_particle = []
        if 'Processing event' in line:
            particles.append(this_particle[:-1])
            if first_particle:
                all_events_particles.append(particles[1:])
                first_particle = False
            else:
                all_events_particles.append(particles)
            particles = []
            this_particle = []
            ignore_line = True
        if not ignore_line:
            this_particle.append(line)
        else:
            ignore_line = False
        prev_line = line
    if len(all_events_particles) == 1:
        print('Single Event in file')
        return all_events_particles[0]
    else:
        print('{0} Events in file'.format(len(all_events_particles)))
        return all_events_particles


def create_event_dicts(particle_list, to_save=False, save_name='test'):
    """
    Turn the strings extracted in :code:`extract_event_log_information` into dictionaries.
    It can only handle one event at a time so can be run with multiprocessing for each event (list) within a list

    :param particle_list: particle information as a string
    :type particle_list: list[list[str]]
    :param to_save: if True, save list to NPY
    :type to_save: bool
    :param save_name: name of save file (include type)
    :type save_name: str
    :return: A dictionary for each particle in an event
    :rtype: list[dict[str, int, int, list[float],
    list[float], list[float], list[float], list[float], list[str], list[str]]]
    """
    all_data_list = []
    for str_list in particle_list:
        particle_info = {}
        x_steps = []
        y_steps = []
        z_steps = []
        kin_e_steps = []
        dep_e_steps = []
        process_steps = []
        volume_steps = []
        step_number = 0
        process_lines = False
        for line in str_list:
            if line == '\n':
                process_lines = False
            if 'Particle' in line:
                if 'ParticleChange' not in line:
                    try:
                        split_line = line.split()
                        particle = split_line[5][:-1]
                        track_id = int(split_line[9][:-1])
                        parent_id = int(split_line[-1])
                        step_number = 0
                    except:
                        print(line)
                        break

            if process_lines:
                split_line = line.split()
                if len(split_line) == 10:
                    if step_number == int(split_line[0]):
                        x_steps.append(float(split_line[1]))
                        y_steps.append(float(split_line[2]))
                        z_steps.append(float(split_line[3]))
                        kin_e_steps.append(float(split_line[4]))
                        dep_e_steps.append(float(split_line[5]))
                        volume_steps.append(split_line[8])
                        process_steps.append(split_line[9])
                        step_number += 1

            if 'Step#' in line:
                process_lines = True

        particle_info['particle'] = particle
        particle_info['track_id'] = track_id
        particle_info['parent_id'] = parent_id
        particle_info['x_mm'] = x_steps
        particle_info['y_mm'] = y_steps
        particle_info['z_mm'] = z_steps
        particle_info['Kin_MeV'] = kin_e_steps
        particle_info['dE_MeV'] = dep_e_steps
        particle_info['NextVolume'] = volume_steps
        particle_info['ProcName'] = process_steps
        all_data_list.append(particle_info)
    if to_save:
        np.save(save_name, all_data_list)
    return all_data_list


def extract_particle_from_dict(npy_names, particle='neutron', merge_multiples=True, add_filename=True):
    """
    This function returns all information about a given particle type for each event.
    For the GdLS study it is only useful for neutrons as it will only return one per event.
    However it has been written in a generalised way so that it can be used with other simulations in the future.

    If :code:`merge_multiples == True`, then the particle will be merged into a single dictionary. This was put in for
    neutrons, where the number of steps often goes above 50 and so is split into two separate entries. For other
    particles this option should probably not be used (unless they are primary particles - ie what started the
    simulation).

    :param npy_names: list of strings containing the paths to NPY files.
    :type npy_names: list[str]
    :param particle: Particle to extract data of. eg 'neutron'
    :type particle: str
    :param merge_multiples: If True, will return a list[dict]. If False, will return a list[list[dict]]
    :type merge_multiples: bool
    :param add_filename: If True, add key entry pointing to the filename
    :type add_filename: bool
    :return: dictionaries of the particles
    :rtype: list[dict] or list[list[dict]]
    """
    particle_dictionary_list = []

    start_time = time.time()
    n_files = len(npy_names)
    progress_bars.progress_update(0, n_files, start_time)
    # Loop over each NPY file
    for j, file_name in enumerate(npy_names):
        event_list = np.load(file_name, allow_pickle=True)
        this_event_dictionaries = []
        for particle_dictionary in event_list:
            if particle_dictionary['particle'] == particle:
                this_event_dictionaries.append(particle_dictionary)
        del event_list
        # Now check to see the number of neutron entries
        if merge_multiples:
            if len(this_event_dictionaries) == 1:
                if add_filename:
                    this_event_dictionaries[0]['filename'] = file_name
                particle_dictionary_list.append(this_event_dictionaries[0])
            else:
                # Merge dictionaries together
                merged_dictionary = {}
                for key in this_event_dictionaries[0].keys():
                    key_data = []
                    for particle_dictionary in this_event_dictionaries:
                        data = particle_dictionary[key]
                        key_data.append(data)
                    if type(key_data[0]) == list:
                        key_data = [entry for sub_list in key_data for entry in sub_list]
                    elif key == 'particle':
                        key_data = key_data[0]
                    else:
                        key_data = key_data
                    merged_dictionary[key] = key_data
                if add_filename:
                    merged_dictionary['filename'] = file_name
                particle_dictionary_list.append(merged_dictionary)
        else:
            if add_filename:
                this_event_dictionaries['filename'] = file_name
            particle_dictionary_list.append(this_event_dictionaries)

        progress_bars.progress_update(j + 1, n_files, start_time)
    return particle_dictionary_list


def get_particle_secondaries(primary_particle_dictionary, filter_by_volume=[], ignore_self=True):
    """
    Get the secondaries produced by a particle

    :param primary_particle_dictionary:
    :type primary_particle_dictionary:
    :param filter_by_volume: Volumes to get secondaries from
    :type filter_by_volume: list[str]
    :param ignore_self: if True, ignore the secondary if it has the same name as the primary
    :type ignore_self: bool
    :return:
    :rtype: list[str]
    """

    particle_secondaries = []

    try:
        file = primary_particle_dictionary['filename']
    except:
        print("filename does not exist, load the particle using extract_particle_from_dict with add_filename=True or"
              "do it manually")
        return []

    # Can either be a list or a str depending on if merged or not
    primary_track_id = primary_particle_dictionary['track_id']

    # Load event dictionaries
    event_list = np.load(file, allow_pickle=True)

    if ignore_self:
        ignore_particle = primary_particle_dictionary['particle']
    else:
        ignore_particle = ''

    # Get list[str] of secondaries
    if type(primary_track_id) == list:
        for track_id in primary_track_id:
            secondaries = get_track_id_quantity(event_list, track_id, filter_by_volume, ignore_particle, 'particle')
            particle_secondaries.extend(secondaries)
    else:
        secondaries = get_track_id_quantity(event_list, primary_track_id, filter_by_volume, ignore_particle, 'particle')
        particle_secondaries.extend(secondaries)
    return particle_secondaries


def get_track_id_quantity(event_list, track_id, filter_by_volume, ignore_particle, return_key):
    """
    Get a quantity from dictionaries which have a parent_id equal to the track_id passed through.

    .. todo::
        Generalise to give the searching feature?

    :param event_list:
    :type event_list: list[dict]
    :param track_id:
    :type track_id: int
    :param filter_by_volume:
    :type filter_by_volume: list[str]
    :param ignore_particle: particle to ignore
    :type ignore_particle: str
    :param return_key:
    :type return_key: str
    :return:
    :rtype:
    """
    track_info = []
    for particle_dictionary in event_list:
        correct_particle = False
        if particle_dictionary['parent_id'] == track_id:
            if particle_dictionary['particle'] != ignore_particle:
                if len(filter_by_volume) == 0:
                    correct_particle = True
                else:
                    for volume in filter_by_volume:
                        if particle_dictionary['NextVolume'][0] == volume:
                            correct_particle = True
        if correct_particle:
            track_info.append(particle_dictionary[return_key])
    return track_info


def get_neutron_capture_isotope(primary_particle_dictionary):
    """
    Get the isotope which captured the neutron.

    Attempt to ignore non-capture particles; gamma, electron, carbon 13 and carbon 14
    which have the same XYZ as the capture.

    This function does not care about volumes, so the neutron end location should be filtered before running this
    function.

    :param primary_particle_dictionary: dict
    :type primary_particle_dictionary: neutron dictionary
    :return: capture isotope
    :rtype: str
    """

    # Can either be a list or a str depending on if merged or not
    primary_track_id = primary_particle_dictionary['track_id']
    if type(primary_track_id) == list:
        track_id = primary_track_id[-1]
    else:
        track_id = primary_track_id
    primary_xyz = [primary_particle_dictionary['x_mm'][-1], primary_particle_dictionary['y_mm'][-1],
                   primary_particle_dictionary['z_mm'][-1]]

    # Load event dictionaries
    event_list = np.load(primary_particle_dictionary['filename'], allow_pickle=True)

    for particle_dictionary in event_list:
        if particle_dictionary['parent_id'] == track_id:
            # check x-y-z
            particle_xyz = [particle_dictionary['x_mm'][0], particle_dictionary['y_mm'][0],
                            particle_dictionary['z_mm'][0]]
            if particle_xyz == primary_xyz:
                # Ignore gamma and electrons and carbon
                particle = particle_dictionary['particle']
                if particle != 'gamma' and particle != 'e-' and particle != 'C13' and particle != 'C14':
                    return particle_dictionary['particle']



