GdLS_Physics
============

This is the git repository for looking at GdLS Physics.

Aims
----
The primary aims of this work is to understand what is actually happening in the
Outer Detector.
Specifically, neutrons, and what 4.7 gammas actually looks like.
This is being understood by;

1. Create cascade trees
2. Energy distribution of neutrons

To Use
------

.. code-block:: sh

   git clone git@gitlab.com:LZ_Projects/Veto/gdls_physics.git
   cd gdls_physics
   conda create -f environment.yml
   conda activate gdls_physics

Notebooks
---------
The analysis work is performed in notebooks.

**Analysis_Notebook.ipynb:** is generally a test notebook for now but does contain some trees.

**DD_neutron_propagation.ipynb:** looks at how neutrons propagate around the detector before they are captured.
It uses the code from :code:`baccarat_verbose_reader.py`.

**Gadolinium_DICEBOX_simulation_analysis.ipynb:** looks at the DICEBOX simulation output.
This is how gadolinium responds to capturing a neutron.
It uses the code from :code:`dicebox_simulation_reader.py`.

Full Documentation
------------------
Full documentation and physics writeup can be found `here <https://lz_projects.gitlab.io/Veto/gdls_physics/>`_.

