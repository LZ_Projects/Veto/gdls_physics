Useful Information
==================

Live document
-------------

I've started collating this information in full (including plots) into an overleaf document;

`Overleaf summary <https://www.overleaf.com/read/wdtgxxkrgrmy>`_


Presentations
-------------
Below are the links to presentations I have made on this;


01 March 2021 - Michigan Analysis Meeting

* `share link 3 <https://uob-my.sharepoint.com/:p:/g/personal/ak18773_bristol_ac_uk/EZRa65yzdnFNi55e5lMhKMIBM0iU-DotWEOurHp-scBMtg?e=GhWEy2>`_
* `personal link 3 <https://uob-my.sharepoint.com/:p:/r/personal/ak18773_bristol_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7B9CEB5A94-76B3-4D71-8B9E-5EE6532128C2%7D&file=dd_neutron_physics_01Mar2021.pptx&action=edit&mobileredirect=true>`_

25 Feb 2021 - Call w/ B

* `share link 2 <https://uob-my.sharepoint.com/:p:/g/personal/ak18773_bristol_ac_uk/EeaX9aufqmtAgAY6elQ3xl0BNflGTx4cbh9SZUSDUjSyMA?e=uLNsT8>`_
* `personal link 2 <https://uob-my.sharepoint.com/:p:/r/personal/ak18773_bristol_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7BABF597E6-AA9F-406B-8006-3A7A5437C65D%7D&file=Neutrons_25Feb2021.pptx&action=edit&mobileredirect=true>`_

05 Feb 2021 - exploring truth

* `personal link 1 <https://uob-my.sharepoint.com/:p:/r/personal/ak18773_bristol_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7BB0EF7CF4-A452-4811-A28C-17D9803D1AD7%7D&file=Neutrons_5Feb2021.pptx&action=edit&mobileredirect=true>`_


Papers
------

Here is a list of useful GdLS physics;

1. `Gamma-ray speectrum from neutron capture on Gd <https://arxiv.org/pdf/1809.02664.pdf>`_
2. `Details about modelling in Geant4 <https://zzz.physics.umn.edu/!lowrad/_media/meeting8/ychen_gdgammas_aarm2015.pdf>`_
3. `electrons induced <https://nars.osu.edu/sites/nars.osu.edu/files/uploads/cao-15-measurement_of_internal_conversion_electrons_from_gd_neutron_capture.pdf>`_
4. `Scintillation decay time <https://arxiv.org/pdf/1102.0797.pdf>`_


