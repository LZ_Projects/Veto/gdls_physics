.. include:: ../README.rst
   :end-before: Full Documentation

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   useful_papers

.. toctree::
   :maxdepth: 1
   :caption: Code reference
   :glob:

   api/*
